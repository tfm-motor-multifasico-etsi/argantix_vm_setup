#!/bin/bash

#Actualiza flatpaks
flatpak update

#Revisa si hay actualizaciones, se las instala, y quita el contenido antiguo
sudo apt update && sudo apt upgrade && sudo apt autoremove
