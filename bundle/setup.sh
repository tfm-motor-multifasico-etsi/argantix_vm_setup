#!/bin/bash

#Actualizar sistema
sudo apt update
sudo apt upgrade 
#installar flatpak, con el plugin para el store
sudo apt install flatpak
sudo apt install gnome-software-plugin-flatpak

# Añardir flathub como tienda
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# cambiar los permisos para que no te pilles los dedos y corras el que no es
chmod -x ~/bundle/setup.sh
chmod +x ~/bundle/setup_2.sh

# Hacer que se pueda updatear el sistema completo con ejecutar esto
cp ~/bundle/update.sh ~/update.sh
chmod +x ~/update.sh

# hacer una copia para no jorobar
cp -r ~/bundle/ARGKDCGui ~/Documents/ARGKDCGui

# Apagar
shutdown now
