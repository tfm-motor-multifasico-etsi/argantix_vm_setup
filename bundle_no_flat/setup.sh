#!/bin/bash
sudo dpkg --add-architecture i386
sudo apt update
sudo apt upgrade 
sudo apt install wine

cp ~/bundle_no_flat/update.sh ~/update.sh
chmod +x ~/update.sh

cp ~/bundle_no_flat/Argantix.desktop ~/.local/share/applications/Argantix.desktop
chmod +x ~/.local/share/applications/Argantix.desktop
shutdown now
